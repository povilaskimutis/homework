# Feedback Frontend

Feedback frontend.


Convert Quiz into Test Form
#1 add correct answer to each question
#2 on submit check how many questions were answered correctly
#3 display result with new component
#4 fix bug with question staying anwered when all checkboxes are removed or input field cleared
#5 expand validation (e.g. add possibility to set how many checkboxes are required to make question answered)
#6 write test for RadioAnswer component, which checks that only 1 checkbox can be selected in a group
