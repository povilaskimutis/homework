import React from 'react';
import { shallow } from 'enzyme';

import Button from 'components/Button/Button';

describe('Button', () => {
    it('should have text', () => {
        const button = shallow(<Button text="Button Text" />);
        const text = button.text();

        expect(text).toEqual('Button Text');
    });
});
