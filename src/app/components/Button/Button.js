import React from 'react';
import PropTypes from 'prop-types';

class Button extends React.Component {
    onClick = () => {
        console.log('click2: ', Math.random());
    };

    render() {
        const { text } = this.props;

        return (
            <button onClick={this.onClick} type="button">{text}</button>
        );
    }
}

Button.propTypes = {
    text: PropTypes.string.isRequired,
};

export default Button;
