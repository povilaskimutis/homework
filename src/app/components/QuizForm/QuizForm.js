import React from 'react';
import PropTypes from 'prop-types';
import Button from '@material-ui/core/Button/Button';

import Header from 'components/Header/Header';
import Question from 'components/Question/Question';

import './quizForm.scss';
import Typography from '@material-ui/core/Typography/Typography';

class QuizForm extends React.Component {
    constructor(props) {
        super(props);

        ///sharina variable tarp funkciju 1 dalis
        this.setSomeVariable = this.setSomeVariable.bind(this);
        ///
        this.state = {
            formValid: true,
            formCorrect: null,
        };
    }

    ///sharina variable tarp funkciju 2 dalis
    setSomeVariable(propertyTextToAdd) {
        this.setState({
            myProperty: propertyTextToAdd
        });
    }
    ////////
    /* eslint-disable react/sort-comp */
    feedback;
    /* eslint-enable */

    componentDidMount = () => {
        this.feedback = {
            quizId: this.props.quiz.id,
            answers: {},
        };
    };

    handleChange = (id, value) => {
        this.feedback.answers[`question-${id}`] = value;
    };

    isFormValid = () => {
        const { quiz } = this.props;
        const { answers } = this.feedback;
        console.log('quiz.questions', quiz.questions);
        console.log('answers', answers);
        console.log('answers.length', Object.keys(answers).length);
        return quiz.questions.length === Object.keys(answers).length;
    }



///patikria ar teisingi atsakymai kai galioja salygos
   isFormCorrect = () => {
           const { quiz } = this.props;
           const { answers } = this.feedback;
           ////teisingu ats skaicius
           let correctAnswers = 0;
           if (answers[`question-1`] == "1"){
           correctAnswers++;
           }
            if (answers[`question-2`] == "cuse"){
                      correctAnswers++;
                      }
                       if (answers[`question-3`] == "dog"){
                                 correctAnswers++;
                                 }
           console.log("corect asw", correctAnswers);
           this.setSomeVariable(correctAnswers);

//// cia eina visos salygos, norint  patikrinti, ar visi klausimai teisingi

           return answers[`question-1`] == "1" && answers[`question-2`] == "cuse" && answers[`question-3`] == "dog";
       }


    handleSubmit = (e) => {
        e.preventDefault();
        const isValid = this.isFormValid();
        const isCorrect = this.isFormCorrect();
        if (isValid) {
                 this.props.submitForm(this.feedback);
             }
        if (isCorrect) {
                                  this.props.submitForm(this.feedback);
                              }
             this.setState({ formValid: isValid });
             this.setState({ formCorrect: isCorrect });
             };

    render() {
        const { isLoading, error, quiz } = this.props;


        return (
            <form className="quiz-form" noValidate onSubmit={this.handleSubmit}>
                <Header text={quiz.title} />
                {error && (
                    <Typography className="description" variant="h5" color="error">
                        {error}
                    </Typography>
                )}
                {!this.state.formValid && (
                    <Typography className="description" variant="h5" color="error">
                        Not all questions have been answered.
                    </Typography>
                )}
               {!this.state.formCorrect &&  this.state.formCorrect!=null  && (
                                   <Typography className="description" variant="h5" color="error">
                                       You have wrong answers! Correct answers: {this.state.myProperty}
                                   </Typography>
                               )}
                                {this.state.formCorrect   && (
                                                                  <Typography className="description" variant="h5" color="error">
                                                                      All are correct! Correct answers: {this.state.myProperty}
                                                                  </Typography>
                                                              )}




                <Typography className="description" variant="h5" color="inherit">
                    {quiz.description}
                </Typography>
                {quiz.questions && quiz.questions.map(q => (
                    <Question
                        key={q.id}
                        data={q}
                        onChange={value => this.handleChange(q.id, value)}
                    />
                ))}
                <div className="control-section">
                    <Button
                        disabled={isLoading || !(quiz.id)}
                        variant="contained"
                        color="primary"
                        onClick={this.handleSubmit}
                        type="submit"
                    >
                        {isLoading ? 'Saving...' : 'Submit'}
                    </Button>
                    <Button variant="contained" type="button">
                        Cancel
                    </Button>
                </div>
            </form>
        );
    }
}

QuizForm.propTypes = {
    isLoading: PropTypes.bool.isRequired,
    error: PropTypes.string.isRequired,
    quiz: PropTypes.shape({
        id: PropTypes.string,
        code: PropTypes.string,
        title: PropTypes.string,
        description: PropTypes.string,
        active: PropTypes.bool,
        questions: PropTypes.arrayOf(PropTypes.shape({
            id: PropTypes.number,
            text: PropTypes.string,
            type: PropTypes.string,
            answers: PropTypes.arrayOf(PropTypes.shape({
                id: PropTypes.number,
                text: PropTypes.string,
            })),
        })),
    }).isRequired,
    submitForm: PropTypes.func.isRequired,
};

export default QuizForm;
